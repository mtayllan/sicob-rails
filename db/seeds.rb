# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Center.create(name: 'Centro de Tecnologia', initials: 'CT')

u1 = User.create(
  email: 'Usuario1@sicob.com', password: '123456',
  center_id: 1, username: 'usuario1'
)

u2 = User.create(
  email: 'usuario2@sicob.com', password: '123456',
  center_id: 1, username: 'usuario2'
)

Category.create(name: 'Eletrônicos')
Category.create(name: 'Móveis')
Category.create(name: 'Mesa')


5.times do
  Product.create(name: Faker::Commerce.product_name, patrimony_number: '123456',
    user_id: 1, category_id: [1,2,3].sample)
end

5.times do
  Product.create(name: Faker::Commerce.product_name, patrimony_number: '123456',
    user_id: 2, category_id: [1,2,3].sample)
end

Product.where(user_id: 1).each do |prod|
  Solicitation.create(user_id: 2, product_id: prod.id, status: [0,1,2].sample)
end