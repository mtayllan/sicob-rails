class CreateCenters < ActiveRecord::Migration[5.2]
  def change
    create_table :centers do |t|
      t.string :name
      t.string :initials
      t.string :campus
      t.string :website

      t.timestamps
    end
  end
end
