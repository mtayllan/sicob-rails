Rails.application.routes.draw do
  resources :solicitations
  resources :products
  devise_for :users
  resources :centers
  resources :categories
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
